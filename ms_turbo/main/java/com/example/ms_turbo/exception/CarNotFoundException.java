package com.example.ms_turbo.exception;

public class CarNotFoundException extends RuntimeException  {
    public CarNotFoundException(String message){
        super(message);
    }

}
