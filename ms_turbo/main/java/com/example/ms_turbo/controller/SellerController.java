package com.example.ms_turbo.controller;

import com.example.ms_turbo.dto.SellerRequestDto;
import com.example.ms_turbo.dto.SellerResponseDto;
import com.example.ms_turbo.service.SellerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/seller/")
@RestController
@RequiredArgsConstructor

public class SellerController {
    private final SellerService sellerService;

    @GetMapping("/id/{id}")
    public SellerResponseDto getById(@PathVariable(name = "id") Long id) {
        return sellerService.getById(id);
    }

    @PostMapping()
    public SellerResponseDto saveSeller(@RequestBody SellerRequestDto sellerRequestDto) {
        return sellerService.saveSeller(sellerRequestDto);
    }

    @PostMapping("/save-all")
    public List<SellerResponseDto> saveAllSeller(@RequestBody List<SellerRequestDto> sellerRequestDtoList) {
        return sellerService.saveAllSeller(sellerRequestDtoList);
    }

}



