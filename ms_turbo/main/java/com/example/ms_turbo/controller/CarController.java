package com.example.ms_turbo.controller;
import com.example.ms_turbo.dto.CarRequestDto;
import com.example.ms_turbo.dto.CarResponseDto;
import com.example.ms_turbo.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/car/")
@RestController
@RequiredArgsConstructor
public class CarController {
    private final CarService carService;


    @GetMapping("/id/{id}")
    public CarResponseDto findById(@PathVariable(name = "id") Long id) {
        return carService.findById(id);

    }

    @PostMapping("save/id/{sellerId}")
    public CarResponseDto saveCar(@PathVariable Long sellerId, @RequestBody CarRequestDto carRequestDto) {
        return carService.saveCar(sellerId, carRequestDto);
    }

    @PutMapping("/update/id/{id}")
    public CarResponseDto update(@PathVariable(name = "id") Long id, @RequestBody CarRequestDto carRequestDto) {
        return carService.updateCar(id, carRequestDto);
    }

    @GetMapping("/find-all")
    public List<CarResponseDto> findAll() {
        return carService.findAll();
    }

    @DeleteMapping("/id/{id}")
    public String deleteCar(@PathVariable Long id) {
        return carService.deleteCar(id);

    }


//    Filter

    @GetMapping("/city/{city}")
    public List<CarResponseDto> getCarsByCity(@PathVariable String city) {
        return carService.findByCity(city);
    }

    @GetMapping("/brand/{brand}")
    public List<CarResponseDto> getCarsByBrand(@PathVariable String brand) {
        return carService.findByBrand(brand);
    }

    @GetMapping("/model/{model}")
    public List<CarResponseDto> getCarsByModel(@PathVariable String model) {
        return carService.findByModel(model);
    }

    @GetMapping("/year/{year}")
    public List<CarResponseDto> getCarsByYear(@PathVariable Integer year) {
        return carService.findByYear(year);
    }

    @GetMapping("/bodyType/{bodyType}")
    public List<CarResponseDto> getCarsByBodyType(@PathVariable String bodyType) {
        return carService.findByBodyType(bodyType);
    }

    @GetMapping("/color/{color}")
    public List<CarResponseDto> getCarsByColor(@PathVariable String color) {
        return carService.findByColor(color);
    }

    @GetMapping("/engine/{engine}")
    public List<CarResponseDto> getCarsByEngine(@PathVariable String engine) {
        return carService.findByEngine(engine);
    }

    @GetMapping("/mileage/{mileage}")
    public List<CarResponseDto> getCarsByMileage(@PathVariable Integer mileage) {
        return carService.findByMileage(mileage);
    }

    @GetMapping("/transmission/{transmission}")
    public List<CarResponseDto> getCarsByTransmission(@PathVariable String transmission) {
        return carService.findByTransmission(transmission);
    }

    @GetMapping("/drive/{drive}")
    public List<CarResponseDto> getCarsByDrive(@PathVariable String drive) {
        return carService.findByDrive(drive);
    }

    @GetMapping("/condition/{condition}")
    public List<CarResponseDto> getCarsByCondition(@PathVariable String condition) {
        return carService.findByCondition(condition);
    }

    @GetMapping("/seats/{seats}")
    public List<CarResponseDto> getCarsBySeats(@PathVariable Integer seats) {
        return carService.findBySeats(seats);
    }

    @GetMapping("/accidents/{accidents}")
    public List<CarResponseDto> getCarsByAccidents(@PathVariable String accidents) {
        return carService.findByAccidents(accidents);
    }

    @GetMapping("/market/{market}")
    public List<CarResponseDto> getCarsByMarket(@PathVariable String market) {
        return carService.findByMarket(market);
    }
}



