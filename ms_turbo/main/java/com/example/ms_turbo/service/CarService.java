package com.example.ms_turbo.service;

import com.example.ms_turbo.dto.CarRequestDto;
import com.example.ms_turbo.dto.CarResponseDto;

import java.util.List;

public interface CarService {
    CarResponseDto findById(Long id);

    CarResponseDto saveCar(Long sellerId, CarRequestDto carRequestDto);

    CarResponseDto updateCar(Long id, CarRequestDto carRequestDto);

    List<CarResponseDto> findAll();
    String deleteCar(Long id);


    ////    Filter methods
   List<CarResponseDto>findByCity(String city);
   List<CarResponseDto>findByBrand(String brand);
   List<CarResponseDto>findByModel(String model);
   List<CarResponseDto>findByYear(Integer year);
   List<CarResponseDto>findByBodyType(String bodyType);
   List<CarResponseDto>findByColor(String color);
   List<CarResponseDto>findByEngine(String engine);
   List<CarResponseDto>findByMileage(Integer mileage);
   List<CarResponseDto>findByTransmission(String transmission);
   List<CarResponseDto>findByDrive(String drive);
   List<CarResponseDto>findByCondition(String condition);
   List<CarResponseDto>findBySeats(Integer seats);
   List<CarResponseDto>findByAccidents(String accidents);
   List<CarResponseDto>findByMarket(String market);


}
