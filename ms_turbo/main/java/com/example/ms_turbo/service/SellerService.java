package com.example.ms_turbo.service;

import com.example.ms_turbo.dto.SellerRequestDto;
import com.example.ms_turbo.dto.SellerResponseDto;
import com.example.ms_turbo.model.Seller;

import java.util.List;

public interface SellerService {
    SellerResponseDto getById(Long id);
    SellerResponseDto saveSeller(SellerRequestDto sellerRequestDto);
    List<SellerResponseDto>saveAllSeller(List<SellerRequestDto>sellerRequestDtoList);
}
