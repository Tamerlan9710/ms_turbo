package com.example.ms_turbo.service;

import com.example.ms_turbo.dto.CarRequestDto;
import com.example.ms_turbo.dto.CarResponseDto;
import com.example.ms_turbo.mapper.CarMapper;
import com.example.ms_turbo.model.Car;
import com.example.ms_turbo.model.Seller;
import com.example.ms_turbo.repository.CarRepository;
import com.example.ms_turbo.repository.SellerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {
    private final CarRepository carRepository;
    private final CarMapper carMapper;
    private final SellerRepository sellerRepository;


    @Override
    public CarResponseDto findById(Long id) {
        Optional<Car> car = carRepository.findById(id);
        return carMapper.carToCarResponseDto(car.orElse(new Car()));
    }

    @Override
    public CarResponseDto saveCar(Long sellerId, CarRequestDto carRequestDto) {
        Car car = carMapper.carRequestDtoToCar(carRequestDto);
        Seller seller = sellerRepository.getReferenceById(sellerId);
        car.setSeller(seller);
        carRepository.save(car);

        seller.getCars().add(car);
        sellerRepository.save(seller);
        return carMapper.carToCarResponseDto(car);
    }

    @Override
    public CarResponseDto updateCar(Long id, CarRequestDto carRequestDto) {
        Optional<Car> car = carRepository.findById(id);
        if (car.isPresent()) {
            car.get().setCity(carRequestDto.getCity());
            car.get().setBrand(carRequestDto.getBrand());
            car.get().setModel(carRequestDto.getModel());
            car.get().setYear(carRequestDto.getYear());
            car.get().setBodyType(carRequestDto.getBodyType());
            car.get().setColor(carRequestDto.getColor());
            car.get().setEngine(carRequestDto.getEngine());
            car.get().setMileage(carRequestDto.getMileage());
            car.get().setTransmission(carRequestDto.getTransmission());
            car.get().setDrive(carRequestDto.getDrive());
            car.get().setCondition(carRequestDto.getCondition());
            car.get().setSeats(carRequestDto.getSeats());
            car.get().setAccidents(carRequestDto.getAccidents());
            car.get().setMarket(carRequestDto.getMarket());
            carRepository.save(car.get());
        }
        return carMapper.carRequestDtoToCarResponseDto(carRequestDto);
    }




    @Override
    public List<CarResponseDto> findAll() {
        List<Car> cars = carRepository.findAll();
        return carMapper.carListToCarResponseDtoList(cars);
    }

    @Override
    public String deleteCar(Long id) {
        String message;
        Optional<Car> car = carRepository.findById(id);
        if (car.isPresent()) {
            carRepository.deleteById(id);
            message = "Was deleted car by id equals to";

        } else {
            message = "User not found by id equals to";
        }
        return message + id;
    }


//    Filter Methods

    @Override
    public List<CarResponseDto> findByCity(String city) {
        List<Car> cars = carRepository.findByCity(city);
        return carMapper.carListToCarResponseDtoList(cars);
    }

    @Override
    public List<CarResponseDto> findByBrand(String brand) {
        List<Car> cars = carRepository.findByBrand(brand);
        return carMapper.carListToCarResponseDtoList(cars);
    }

    @Override
    public List<CarResponseDto> findByModel(String model) {
        List<Car> cars = carRepository.findByModel(model);
        return carMapper.carListToCarResponseDtoList(cars);
    }

    @Override
    public List<CarResponseDto> findByYear(Integer year) {
        List<Car> cars = carRepository.findByYear(year);
        return carMapper.carListToCarResponseDtoList(cars);
    }

    @Override
    public List<CarResponseDto> findByBodyType(String bodyType) {
        List<Car> cars = carRepository.findByBodyType(bodyType);
        return carMapper.carListToCarResponseDtoList(cars);
    }

    @Override
    public List<CarResponseDto> findByColor(String color) {
        List<Car> cars = carRepository.findByColor(color);
        return carMapper.carListToCarResponseDtoList(cars);
    }

    @Override
    public List<CarResponseDto> findByEngine(String engine) {
        List<Car> cars = carRepository.findByEngine(engine);
        return carMapper.carListToCarResponseDtoList(cars);
    }


    @Override
    public List<CarResponseDto> findByMileage(Integer mileage) {
        List<Car> cars = carRepository.findByMileage(mileage);
        return carMapper.carListToCarResponseDtoList(cars);
    }

    @Override
    public List<CarResponseDto> findByTransmission(String transmission) {
        List<Car> cars = carRepository.findByTransmission(transmission);
        return carMapper.carListToCarResponseDtoList(cars);
    }

    @Override
    public List<CarResponseDto> findByDrive(String drive) {
        List<Car> cars = carRepository.findByDrive(drive);
        return carMapper.carListToCarResponseDtoList(cars);
    }

    @Override
    public List<CarResponseDto> findByCondition(String condition) {
        List<Car> cars = carRepository.findByCondition(condition);
        return carMapper.carListToCarResponseDtoList(cars);
    }

    @Override
    public List<CarResponseDto> findBySeats(Integer seats) {
        List<Car> cars = carRepository.findBySeats(seats);
        return carMapper.carListToCarResponseDtoList(cars);
    }

    @Override
    public List<CarResponseDto> findByAccidents(String accidents) {
        List<Car> cars = carRepository.findByAccidents(accidents);
        return carMapper.carListToCarResponseDtoList(cars);
    }

    @Override
    public List<CarResponseDto> findByMarket(String market) {
        List<Car> cars = carRepository.findByMarket(market);
        return carMapper.carListToCarResponseDtoList(cars);
    }

}






