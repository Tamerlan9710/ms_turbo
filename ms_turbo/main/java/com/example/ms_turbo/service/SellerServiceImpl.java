package com.example.ms_turbo.service;

import com.example.ms_turbo.dto.SellerRequestDto;
import com.example.ms_turbo.dto.SellerResponseDto;
import com.example.ms_turbo.mapper.SellerMapper;
import com.example.ms_turbo.model.Seller;
import com.example.ms_turbo.repository.SellerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor

public class SellerServiceImpl implements SellerService {
    private final SellerRepository sellerRepository;
    private final SellerMapper sellerMapper;

    @Override
    public SellerResponseDto getById(Long id) {
        return sellerMapper.sellerToSellerResponseDto(sellerRepository.findById(id).get());
   }

    @Override
    public SellerResponseDto saveSeller(SellerRequestDto sellerRequestDto) {
        Seller saveSeller = sellerMapper.sellerRequestDtoToSeller(sellerRequestDto);
        return sellerMapper.sellerToSellerResponseDto(sellerRepository.save(saveSeller));
    }

    @Override
    public List<SellerResponseDto> saveAllSeller(List<SellerRequestDto> sellerRequestDtoList) {
        List<Seller>sellerList=sellerMapper.sellerRequestDtoListToSellerList(sellerRequestDtoList);
        return sellerMapper.sellerListToSellerResponseDtoList(sellerRepository.saveAll(sellerList));
    }
}




