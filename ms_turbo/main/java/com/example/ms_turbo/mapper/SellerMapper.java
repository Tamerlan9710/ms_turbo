package com.example.ms_turbo.mapper;

import com.example.ms_turbo.dto.SellerRequestDto;
import com.example.ms_turbo.dto.SellerResponseDto;
import com.example.ms_turbo.model.Seller;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(unmappedTargetPolicy = IGNORE, componentModel = "Spring")

public interface SellerMapper {

    SellerRequestDto sellerToSellerDto(Seller seller);

    SellerResponseDto sellerToSellerResponseDto(Seller seller);

    Seller sellerResponseDtoToSeller(SellerResponseDto sellerResponseDto);

    Seller sellerRequestDtoToSeller(SellerRequestDto sellerRequestDto);

    List<SellerResponseDto> sellerListToSellerResponseDtoList(List<Seller> sellerList);

    List<Seller> sellerRequestDtoListToSellerList(List<SellerRequestDto> sellerRequestDtoList);


}
