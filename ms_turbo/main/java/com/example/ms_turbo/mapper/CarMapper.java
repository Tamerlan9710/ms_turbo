package com.example.ms_turbo.mapper;

import com.example.ms_turbo.dto.CarRequestDto;
import com.example.ms_turbo.dto.CarResponseDto;
import com.example.ms_turbo.model.Car;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "Spring", unmappedTargetPolicy = IGNORE)

public interface CarMapper {
    CarResponseDto carToCarResponseDto(Car car);

    Car carRequestDtoToCar(CarRequestDto carRequestDto);
    CarResponseDto carRequestDtoToCarResponseDto(CarRequestDto carRequestDto);

    List<CarResponseDto> carListToCarResponseDtoList(List<Car> carList);

    List<Car> carRequestDtoListToCarList(List<CarRequestDto> carRequestDtoList);


}
