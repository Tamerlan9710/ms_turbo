package com.example.ms_turbo.repository;

import com.example.ms_turbo.model.Seller;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SellerRepository extends JpaRepository<Seller,Long> {

}
