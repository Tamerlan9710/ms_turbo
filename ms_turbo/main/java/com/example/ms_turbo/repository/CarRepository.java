package com.example.ms_turbo.repository;

import com.example.ms_turbo.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CarRepository extends JpaRepository<Car,Long> {

    @Override
    List<Car> findAll();
    List<Car>findByCity(String city);
    List<Car>findByBrand(String brand);
    List<Car>findByModel(String model);
    List<Car>findByYear(Integer year);
    List<Car>findByBodyType(String bodyType);
    List<Car>findByColor(String color);
    List<Car>findByEngine(String engine);
    List<Car>findByMileage(Integer mileage);
    List<Car>findByTransmission(String transmission);
    List<Car>findByDrive(String drive);
    List<Car>findByCondition(String condition);
    List<Car>findBySeats(Integer seats);
    List<Car>findByAccidents(String accidents);
    List<Car>findByMarket(String market);


}
