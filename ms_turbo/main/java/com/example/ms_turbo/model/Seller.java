package com.example.ms_turbo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@Entity
@Table
@RequiredArgsConstructor
@ToString
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Seller {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "seller_id", nullable = false)
    Long id;
    String name;
    String surName;
    String phoneNumber;
    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.DETACH,orphanRemoval =true)
    @JsonIgnore
    List<Car> cars;


}
