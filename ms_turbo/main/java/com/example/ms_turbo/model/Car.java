package com.example.ms_turbo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@AllArgsConstructor
@Entity
@Table
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
@Builder

public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Car_id", nullable = false)
    @JsonIgnore
    Long id;
    String city;
    String brand;
    String model;
    Integer year;
    String bodyType;
    String color;
    String engine;
    Integer mileage;
    String transmission;
    String drive;
    String condition;
    Integer seats;
    String accidents;
    String market;
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "seller_id")
    Seller seller;

}
