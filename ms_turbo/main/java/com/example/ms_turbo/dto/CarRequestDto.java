package com.example.ms_turbo.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Component
public class CarRequestDto {
    String city;
    String brand;
    String model;
    Integer year;
    String bodyType;
    String color;
    String engine;
    Integer mileage;
    String transmission;
    String drive;
    String condition;
    Integer seats;
    String accidents;
    String market;
    Long sellerId;

}
