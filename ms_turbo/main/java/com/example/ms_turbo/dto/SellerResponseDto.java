package com.example.ms_turbo.dto;

import com.example.ms_turbo.model.Car;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;
@AllArgsConstructor
@Data
@Builder
@ToString
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SellerResponseDto {
    String name;
    String surName;
    String phoneNumber;
    List<Car>cars;

}
